package tk.itish.confirmedcasesservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableEurekaClient
@EnableScheduling //make the app track the methods to schedule
public class ConfirmedCasesServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConfirmedCasesServiceApplication.class, args);
	}

}
