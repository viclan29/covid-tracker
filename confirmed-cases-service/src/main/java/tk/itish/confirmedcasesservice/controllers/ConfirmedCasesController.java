package tk.itish.confirmedcasesservice.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.itish.confirmedcasesservice.models.CovidStatistics;
import tk.itish.confirmedcasesservice.models.CovidStatisticsList;
import tk.itish.confirmedcasesservice.services.CovidConfirmedService;

import java.io.IOException;
import java.util.List;

//define the class as  rest controller
@RestController
public class ConfirmedCasesController {
    //Get hold of the service
    @Autowired
    CovidConfirmedService covidConfirmedService;

    //get all the confirmed cases wrapped
    @RequestMapping(value = "/v1/cases/confirmed", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public CovidStatisticsList getAllConfirmedCases() throws IOException, InterruptedException {
        return covidConfirmedService.getInfectionsNumber();
    }

    //get all the confirmed cases list
    @RequestMapping(value = "/v2/cases/confirmed", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CovidStatistics> getAllConfirmedCasesList() throws IOException, InterruptedException {
        return covidConfirmedService.getInfectionsNumberList();
    }
}
