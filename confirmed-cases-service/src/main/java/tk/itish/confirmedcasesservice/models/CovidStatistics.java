package tk.itish.confirmedcasesservice.models;

//keep track of the statistics
public class CovidStatistics {
    //create member variables
    private String state;
    private String country;
    private int confirmed;
    private int newCases;

    //create the non-arg constructor
    public CovidStatistics() {
    }

    //create argumented constructor
    public CovidStatistics(String state, String country, int confirmed, int newCases) {
        this.state = state;
        this.country = country;
        this.confirmed = confirmed;
        this.newCases = newCases;
    }

    //create getters and setters
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(int confirmed) {
        this.confirmed = confirmed;
    }

    public int getNewCases() {
        return newCases;
    }

    public void setNewCases(int newCases) {
        this.newCases = newCases;
    }

    @Override
    public String toString() {
        return "CovidStatistics{" +
                "state='" + state + '\'' +
                ", country='" + country + '\'' +
                ", confirmed=" + confirmed +
                '}';
    }
}
