package tk.itish.confirmedcasesservice.services;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import tk.itish.confirmedcasesservice.models.CovidStatistics;
import tk.itish.confirmedcasesservice.models.CovidStatisticsList;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

//initiate a service
@Service
public class CovidConfirmedService {
    //Define the URL to the CSV
    private static String CSV_URL_FILE = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv";

    List<CovidStatistics> oldStaistics = new ArrayList<>();

    //create a method to initiate the csv file
    @PostConstruct //make the body initialize on app start
    @Scheduled(cron = "* * 1 * * *") //schedule the execution of the method to every day
    public CovidStatisticsList getInfectionsNumber() throws IOException, InterruptedException {
        //create a new list based on todays new data
        getConfirmedList();

        //initialize the wrapper
        CovidStatisticsList covidStatisticsList = new CovidStatisticsList();
        covidStatisticsList.setCovidStatisticsList(oldStaistics);
        //return the list
        return covidStatisticsList;
    }

    //get a list of the infections
    @PostConstruct //make the body initialize on app start
    @Scheduled(cron = "* * 1 * * *") //schedule the execution of the method to every day
    public List<CovidStatistics> getInfectionsNumberList() throws IOException, InterruptedException {
        getConfirmedList();

        return oldStaistics;
    }

    private void getConfirmedList() throws IOException, InterruptedException {
        //create a new list based on todays new data
        List<CovidStatistics> newStaistics = new ArrayList<>();

        HttpClient httpClient = HttpClient.newHttpClient();
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .uri(URI.create(CSV_URL_FILE))
                .build();

        //get the response
        HttpResponse<String> httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());

        //parse the csv file here
        //define the reader
        StringReader stringReader = new StringReader(httpResponse.body());
        Iterable<CSVRecord> records = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(stringReader);

        //loop through the response and do something
        for (CSVRecord record : records) {
            //Initilize the statistics model
            CovidStatistics covidStatistics = new CovidStatistics();

            //set model's variables
            covidStatistics.setState(record.get("Province/State"));
            covidStatistics.setCountry(record.get("Country/Region"));
            int confirmed = Integer.parseInt(record.get(record.size() - 1));
            int confirmedPrevious = Integer.parseInt(record.get(record.size() - 2));
            covidStatistics.setConfirmed(confirmed);
            covidStatistics.setNewCases(confirmed - confirmedPrevious);

            //add the list
            newStaistics.add(covidStatistics);
        }

        this.oldStaistics = newStaistics;
    }

    //To aid our testing
    //add numbers
    public static int addNumbers(int num1, int num2) {
        return num1 + num2;
    }

    //find the difference
    public static int subtractNumbers(int num1, int num2) {
        return num1 - num2;
    }

    //find the division
    public static double divideNumbers(int num1, int num2) {
        if (num2 == 2) {
            throw new ArithmeticException("Msee huwezi gawanya kati ya sufuri");
        }
        return num1 / num2;
    }
}
