package tk.itish.covidtracker.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import tk.itish.covidtracker.models.CovidStatistics;
import tk.itish.covidtracker.services.CovidConfirmedService;
import tk.itish.covidtracker.services.CovidDeathsService;
import tk.itish.covidtracker.services.CovidRecoveredService;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

//Create the class as a controller
@Controller
public class CovidController {
    //get the covid service for the confirmed cases
    @Autowired
    CovidConfirmedService covidConfirmedService;

    //get the covid service for the recovered cases
    @Autowired
    CovidRecoveredService covidRecoveredService;

    //get the covid service for the deaths cases
    @Autowired
    CovidDeathsService covidDeathsService;

    //return a html file for the confirmed cases
    @GetMapping("/v1/cases/confirmed")
    public String returnConfirmed(Model model) {
        //define a number format
        NumberFormat numberFormat = NumberFormat.getInstance();
        numberFormat.setGroupingUsed(true);

        //get date format
        DateFormat dateFormat = new SimpleDateFormat("yyyy - MM -dd");

        //fetch all statistics
        List<CovidStatistics> mainStaistics = covidConfirmedService.getAllConfirmed().getCovidStatisticsList();
        model.addAttribute("statistics", mainStaistics);

        //want to get the sum of all the confirmed cases
        int allConfirmedCases = mainStaistics.stream().mapToInt(covidStatistics -> covidStatistics.getConfirmed()).sum();

        //get the sum new cases
        int newCases = mainStaistics.stream().mapToInt(covidStatistics -> covidStatistics.getNewCases()).sum();


        model.addAttribute("allConfirmedCases", numberFormat.format(allConfirmedCases));
        model.addAttribute("allNewCases", numberFormat.format(newCases));
        model.addAttribute("thedate", "As of Date " + dateFormat.format(yesterday()));
        model.addAttribute("statistics", covidConfirmedService.getAllConfirmed().getCovidStatisticsList());

        //return the html file
        return "confirmed";
    }

    //return a html file for the recovered
    @GetMapping("/v1/cases/recovered")
    public String getRecovered(Model model) {
        //define a number format
        NumberFormat numberFormat = NumberFormat.getInstance();
        numberFormat.setGroupingUsed(true);

        //get date format
        DateFormat dateFormat = new SimpleDateFormat("yyyy - MM -dd");

        //get the recovered sum
        int allRecovered = covidRecoveredService.getRecoveredCases().getCovidRecoveredStatistics().stream().mapToInt(covidRecoveredStatistics -> covidRecoveredStatistics.getRecovered()).sum();
        int newRecovered = covidRecoveredService.getRecoveredCases().getCovidRecoveredStatistics().stream().mapToInt(covidRecoveredStatistics -> covidRecoveredStatistics.getNewRecoveredCases()).sum();

        model.addAttribute("thedate", "As of Date " + dateFormat.format(yesterday()));
        model.addAttribute("allRecoveredCases", numberFormat.format(allRecovered));
        model.addAttribute("allRecoveredCasesToday", numberFormat.format(newRecovered));
        model.addAttribute("statistics", covidRecoveredService.getRecoveredCases().getCovidRecoveredStatistics());
        return "recovered";
    }


    //return a file for the deaths
    @GetMapping("/v1/cases/deaths")
    public String getDeaths(Model model) {
        //define a number format
        NumberFormat numberFormat = NumberFormat.getInstance();
        numberFormat.setGroupingUsed(true);

        //get date format
        DateFormat dateFormat = new SimpleDateFormat("yyyy - MM -dd");

        //get the recovered sum
        int allDeaths = covidDeathsService.getAllDeathsCases().getCovidDeathModelsList().stream().mapToInt(covidDeathModel -> covidDeathModel.getAllDeaths()).sum();
        int newDeaths = covidDeathsService.getAllDeathsCases().getCovidDeathModelsList().stream().mapToInt(covidDeathModel -> covidDeathModel.getNewDeaths()).sum();

        model.addAttribute("thedate", "As of Date " + dateFormat.format(yesterday()));
        model.addAttribute("allDeathsCases", numberFormat.format(allDeaths));
        model.addAttribute("allDeathsCasesToday", numberFormat.format(newDeaths));
        model.addAttribute("statistics", covidDeathsService.getAllDeathsCases().getCovidDeathModelsList());
        return "deaths";
    }

    //date method
    private Date yesterday() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return cal.getTime();
    }
}
