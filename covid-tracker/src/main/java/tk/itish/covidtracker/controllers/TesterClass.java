package tk.itish.covidtracker.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.itish.covidtracker.models.CovidStatisticsList;
import tk.itish.covidtracker.services.CovidConfirmedService;

import java.io.IOException;

@RestController
public class TesterClass {
    //Get hold of the service
    @Autowired
    CovidConfirmedService covidConfirmedService;

    //get all the confirmed cases
    @RequestMapping(value = "/v2/cases/confirmed", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public CovidStatisticsList getAllConfirmedCases() throws IOException, InterruptedException {
        return covidConfirmedService.getAllConfirmed();
    }
}
