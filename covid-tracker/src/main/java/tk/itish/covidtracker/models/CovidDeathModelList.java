package tk.itish.covidtracker.models;

import java.util.List;

//define a wrapper for the deaths
public class CovidDeathModelList {
    //member variable
    private List<CovidDeathModel> covidDeathModelsList;

    //constructor
    public CovidDeathModelList(List<CovidDeathModel> covidDeathModelsList) {
        this.covidDeathModelsList = covidDeathModelsList;
    }

    public CovidDeathModelList() {
    }

    //getters and setters
    public List<CovidDeathModel> getCovidDeathModelsList() {
        return covidDeathModelsList;
    }

    public void setCovidDeathModelsList(List<CovidDeathModel> covidDeathModelsList) {
        this.covidDeathModelsList = covidDeathModelsList;
    }
}
