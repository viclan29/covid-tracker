package tk.itish.covidtracker.models;

import java.util.List;

//define a wrapper class
public class CovidRecoveredStatisticsList {
    //define a member variable
    private List<CovidRecoveredStatistics> covidRecoveredStatistics;

    //constructors
    public CovidRecoveredStatisticsList(List<CovidRecoveredStatistics> covidRecoveredStatistics) {
        this.covidRecoveredStatistics = covidRecoveredStatistics;
    }

    public CovidRecoveredStatisticsList() {
    }

    //getters and setters
    public List<CovidRecoveredStatistics> getCovidRecoveredStatistics() {
        return covidRecoveredStatistics;
    }

    public void setCovidRecoveredStatistics(List<CovidRecoveredStatistics> covidRecoveredStatistics) {
        this.covidRecoveredStatistics = covidRecoveredStatistics;
    }
}
