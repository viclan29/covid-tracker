package tk.itish.covidtracker.models;

import java.util.List;

//craete a wrapper class
public class CovidStatisticsList {
    //define member variable
    private List<CovidStatistics> CovidStatisticsList;

    //create constructors
    public CovidStatisticsList() {
    }

    public CovidStatisticsList(List<CovidStatistics> covidStatisticsList) {
        CovidStatisticsList = covidStatisticsList;
    }

    //getters and setters
    public tk.itish.covidtracker.models.CovidStatisticsList getCovidStatisticsList() {
        return CovidStatisticsList;
    }

    public void setCovidStatisticsList(List<CovidStatistics> covidStatisticsList) {
        CovidStatisticsList = covidStatisticsList;
    }
}
