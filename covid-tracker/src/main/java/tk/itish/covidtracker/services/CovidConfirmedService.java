package tk.itish.covidtracker.services;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import tk.itish.covidtracker.models.CovidStatistics;
import tk.itish.covidtracker.models.CovidStatisticsList;

import java.util.Collections;

//initialize as a service
@Service
public class CovidConfirmedService {
    //get the rest template
    @Autowired
    private RestTemplate restTemplate;

    //call the confirmed end point using a rest template
    @HystrixCommand(fallbackMethod = "getAllConfirmedFallback")
    public CovidStatisticsList getAllConfirmed() {
       return restTemplate.getForObject("http://confirmed-cases-service/v2/cases/confirmed", CovidStatisticsList.class);
    }


    //get the fall back method
    public CovidStatisticsList getAllConfirmedFallback() {
        return new CovidStatisticsList(
                Collections.singletonList(new CovidStatistics("No State", "No Country", 0, 0))
        );
    }
}
