package tk.itish.covidtracker.services;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import tk.itish.covidtracker.models.CovidDeathModel;
import tk.itish.covidtracker.models.CovidDeathModelList;

import java.util.Collections;

//define the class as a service
@Service
public class CovidDeathsService {
    //get the rest template
    @Autowired
    private RestTemplate restTemplate;

    //call the deaths end point using a rest template
    @HystrixCommand(fallbackMethod = "getAllDeathsCasesFallback")
    public CovidDeathModelList getAllDeathsCases() {
        return restTemplate.getForObject("http://death-cases-service/v2/cases/deaths", CovidDeathModelList.class);
    }

    //define a fall back method
    public CovidDeathModelList getAllDeathsCasesFallback() {
        return new CovidDeathModelList(Collections.singletonList(new CovidDeathModel("No Country", "No State", 0, 0)));
    }
}
