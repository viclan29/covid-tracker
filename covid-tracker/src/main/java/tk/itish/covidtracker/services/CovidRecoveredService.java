package tk.itish.covidtracker.services;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import tk.itish.covidtracker.models.CovidRecoveredStatistics;
import tk.itish.covidtracker.models.CovidRecoveredStatisticsList;

import java.util.Collections;

//define as a service
@Service
public class CovidRecoveredService {
    //get the rest template
    @Autowired
    private RestTemplate restTemplate;

    //call the recovered stuff using rest template
    @HystrixCommand(fallbackMethod = "getRecoveredCasesFallback")
    public CovidRecoveredStatisticsList getRecoveredCases() {
        return restTemplate.getForObject("http://recovered-cases-service/v2/cases/recovered", CovidRecoveredStatisticsList.class);
    }

    //and here goes a fall back for the above method
    public CovidRecoveredStatisticsList getRecoveredCasesFallback() {
        return new CovidRecoveredStatisticsList(Collections.singletonList(new CovidRecoveredStatistics("No State", "No Country", 0, 0)));
    }
}
