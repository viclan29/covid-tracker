package tk.itish.deathcasesservice.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.itish.deathcasesservice.models.CovidDeathModel;
import tk.itish.deathcasesservice.models.CovidDeathModelList;
import tk.itish.deathcasesservice.services.CovidDeathsService;

import java.io.IOException;
import java.util.List;

//define a rest controller
@RestController
public class DeathCasesController {
    //load the covid deaths service
    @Autowired
    CovidDeathsService covidDeathsService;

    //a method to return the wrapped object of deaths
    @RequestMapping(value = "/v1/cases/deaths", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public CovidDeathModelList getAllDeaths() throws IOException, InterruptedException {
        return covidDeathsService.getDeathsCases();
    }

    //a method to return the list of deaths
    @RequestMapping(value = "/v2/cases/deaths", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CovidDeathModel> getAllDeathsList() throws IOException, InterruptedException {
        return covidDeathsService.getDeathsCasesList();
    }
}
