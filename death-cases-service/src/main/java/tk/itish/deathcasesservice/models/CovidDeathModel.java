package tk.itish.deathcasesservice.models;

//this is a model for the deaths
public class CovidDeathModel {
    //define member variables
    private String country;
    private String state;
    private int allDeaths;
    private int newDeaths;

    //define constructors
    public CovidDeathModel() {
    }

    public CovidDeathModel(String country, String state, int allDeaths, int newDeaths) {
        this.country = country;
        this.state = state;
        this.allDeaths = allDeaths;
        this.newDeaths = newDeaths;
    }

    //define getters and setters
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getAllDeaths() {
        return allDeaths;
    }

    public void setAllDeaths(int allDeaths) {
        this.allDeaths = allDeaths;
    }

    public int getNewDeaths() {
        return newDeaths;
    }

    public void setNewDeaths(int newDeaths) {
        this.newDeaths = newDeaths;
    }

    //create a toString method

    @Override
    public String toString() {
        return "CovidDeathModel{" +
                "country='" + country + '\'' +
                ", state='" + state + '\'' +
                ", allDeaths=" + allDeaths +
                ", newDeaths=" + newDeaths +
                '}';
    }
}
