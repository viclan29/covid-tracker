package tk.itish.deathcasesservice.services;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import tk.itish.deathcasesservice.models.CovidDeathModel;
import tk.itish.deathcasesservice.models.CovidDeathModelList;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

@Service //define the class as a service
public class CovidDeathsService {
    //define the file url
    private static String DEATHS_URL = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv";

    List<CovidDeathModel> oldCovidDeathModels = new ArrayList<>();

    //create a method to initiate the csv file
    @PostConstruct //make the body initialize on app start
    @Scheduled(cron = "* * 1 * * *") //schedule the execution of the method to every day
    public CovidDeathModelList getDeathsCases() throws IOException, InterruptedException {
        //create a new list we be using inside the method for concurrency protection
        oldCovidDeathModels = getDeathsCasesList();

        //initialize the wrapper
        CovidDeathModelList covidStatisticsList = new CovidDeathModelList();
        covidStatisticsList.setCovidDeathModelsList(oldCovidDeathModels);

        return covidStatisticsList;
    }

    //return pure list
    @PostConstruct //make the body initialize on app start
    @Scheduled(cron = "* * 1 * * *") //schedule the execution of the method to every day
    public List<CovidDeathModel> getDeathsCasesList() throws IOException, InterruptedException {
        getDeathsMethod();

        return oldCovidDeathModels;
    }

    private void getDeathsMethod() throws IOException, InterruptedException {
        //create a new list we be using inside the method for concurrency protection
        List<CovidDeathModel> newCovidDeathModels = new ArrayList<>();

        //define a http client
        HttpClient httpClient = HttpClient.newHttpClient();

        //build a response
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .uri(URI.create(DEATHS_URL))
                .build();

        //get a response
        HttpResponse<String> httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());

        //parse the csv file here
        //define the reader
        StringReader stringReader = new StringReader(httpResponse.body());
        Iterable<CSVRecord> records = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(stringReader);

        //loop through the returned lines
        for (CSVRecord record : records) {
            //initialize a model
            CovidDeathModel covidDeathModel = new CovidDeathModel();

            //set model's variables
            covidDeathModel.setState(record.get("Province/State"));
            covidDeathModel.setCountry(record.get("Country/Region"));
            int confirmed = Integer.parseInt(record.get(record.size() - 1));
            int confirmedPrevious = Integer.parseInt(record.get(record.size() - 2));
            covidDeathModel.setAllDeaths(confirmed);
            covidDeathModel.setNewDeaths(confirmed - confirmedPrevious);

            //append to the list
            newCovidDeathModels.add(covidDeathModel);
        }

        this.oldCovidDeathModels = newCovidDeathModels;
    }
}
