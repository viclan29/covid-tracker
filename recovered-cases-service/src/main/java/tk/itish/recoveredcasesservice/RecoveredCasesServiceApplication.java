package tk.itish.recoveredcasesservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableEurekaClient //define the class as a client to the eureka server
@EnableScheduling //make the app track the methods to schedule
public class RecoveredCasesServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(RecoveredCasesServiceApplication.class, args);
    }

}
