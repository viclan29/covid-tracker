package tk.itish.recoveredcasesservice.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.itish.recoveredcasesservice.models.CovidRecoveredStatistics;
import tk.itish.recoveredcasesservice.models.CovidRecoveredStatisticsList;
import tk.itish.recoveredcasesservice.services.CovidRecoveredService;

import java.io.IOException;
import java.util.List;

//define the class as a rest controller
@RestController
public class RecoveredCasesController {
    //get the recovered service
    @Autowired
    CovidRecoveredService covidRecoveredService;

    //the wrapped list of the recovered
    @RequestMapping(value = "/v1/cases/recovered", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public CovidRecoveredStatisticsList getRecoveredCases() throws IOException, InterruptedException {
        return covidRecoveredService.getRecoveredCases();
    }

    //the list of the recovered
    @RequestMapping(value = "/v2/cases/recovered", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CovidRecoveredStatistics> getRecoveredCasesList() throws IOException, InterruptedException {
        return covidRecoveredService.getRecoveredCasesList();
    }
}
