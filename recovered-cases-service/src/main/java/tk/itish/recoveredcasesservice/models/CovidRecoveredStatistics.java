package tk.itish.recoveredcasesservice.models;

//model for the recovered infections
public class CovidRecoveredStatistics {
    //define member variables
    private String state;
    private String country;
    private int recovered;
    private int newRecoveredCases;

    //define constructors
    public CovidRecoveredStatistics() {
    }

    public CovidRecoveredStatistics(String state, String country, int recovered, int newRecoveredCases) {
        this.state = state;
        this.country = country;
        this.recovered = recovered;
        this.newRecoveredCases = newRecoveredCases;
    }

    //getters and setters
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getRecovered() {
        return recovered;
    }

    public void setRecovered(int recovered) {
        this.recovered = recovered;
    }

    public int getNewRecoveredCases() {
        return newRecoveredCases;
    }

    public void setNewRecoveredCases(int newRecoveredCases) {
        this.newRecoveredCases = newRecoveredCases;
    }

    //tostring method

    @Override
    public String toString() {
        return "CovidRecoveredStatistics{" +
                "state='" + state + '\'' +
                ", country='" + country + '\'' +
                ", recovered=" + recovered +
                ", newRecoveredCases=" + newRecoveredCases +
                '}';
    }
}
