package tk.itish.recoveredcasesservice.services;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import tk.itish.recoveredcasesservice.models.CovidRecoveredStatistics;
import tk.itish.recoveredcasesservice.models.CovidRecoveredStatisticsList;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

@Service //define the class as a service
public class CovidRecoveredService {
    //define the file url
    private static String RECOVERED_URL = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_recovered_global.csv";


    List<CovidRecoveredStatistics> oldCovidRecoveredStatistics = new ArrayList<>();

    //create a method to initiate the csv file
    @PostConstruct //make the body initialize on app start
    @Scheduled(cron = "* * 1 * * *") //schedule the execution of the method to every day
    public CovidRecoveredStatisticsList getRecoveredCases() throws IOException, InterruptedException {
        //create a new list we be using inside the method for concurrency protection
        getRecoveredList();

        //initialize the wrapper
        CovidRecoveredStatisticsList covidStatisticsList = new CovidRecoveredStatisticsList();
        covidStatisticsList.setCovidRecoveredStatistics(oldCovidRecoveredStatistics);

        return covidStatisticsList;
    }

    //get an unwrapped list
    @PostConstruct //make the body initialize on app start
    @Scheduled(cron = "* * 1 * * *") //schedule the execution of the method to every day
    public List<CovidRecoveredStatistics> getRecoveredCasesList() throws IOException, InterruptedException {
        getRecoveredList();

        return oldCovidRecoveredStatistics;
    }

    private void getRecoveredList() throws IOException, InterruptedException {
        //create a new list we be using inside the method for concurrency protection
        List<CovidRecoveredStatistics> newCovidRecoveredStatistics = new ArrayList<>();

        //define a http client
        HttpClient httpClient = HttpClient.newHttpClient();

        //build a response
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .uri(URI.create(RECOVERED_URL))
                .build();

        //get a response
        HttpResponse<String> httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());

        //parse the csv file here
        //define the reader
        StringReader stringReader = new StringReader(httpResponse.body());
        Iterable<CSVRecord> records = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(stringReader);

        //loop through the returned lines
        for (CSVRecord record : records) {
            //initialize a model
            CovidRecoveredStatistics covidRecoveredStatistics = new CovidRecoveredStatistics();

            //set model's variables
            covidRecoveredStatistics.setState(record.get("Province/State"));
            covidRecoveredStatistics.setCountry(record.get("Country/Region"));
            int confirmed = Integer.parseInt(record.get(record.size() - 1));
            int confirmedPrevious = Integer.parseInt(record.get(record.size() - 2));
            covidRecoveredStatistics.setRecovered(confirmed);
            covidRecoveredStatistics.setNewRecoveredCases(confirmed - confirmedPrevious);

            //append to the list
            newCovidRecoveredStatistics.add(covidRecoveredStatistics);
        }

        this.oldCovidRecoveredStatistics = newCovidRecoveredStatistics;
    }
}
